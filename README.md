FuelPHP-FTP-Module
===================

Using this FTP Module you can manage files and folders by remotely. This module develop using FuelPHP framework.

Simple setup

![ftp1.png](https://bitbucket.org/repo/xnejEk/images/470441006-ftp1.png)
![ftp2.png](https://bitbucket.org/repo/xnejEk/images/1890270980-ftp2.png)
![ftp3.png](https://bitbucket.org/repo/xnejEk/images/70990803-ftp3.png)